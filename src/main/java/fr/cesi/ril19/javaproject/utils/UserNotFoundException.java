package fr.cesi.ril19.javaproject.utils;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class UserNotFoundException extends ResponseStatusException {

    public UserNotFoundException(Long idUser) {
        super(HttpStatus.NOT_FOUND, "User " + idUser + " doesn't exist!");
    }

}