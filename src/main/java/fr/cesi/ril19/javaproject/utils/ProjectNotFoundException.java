package fr.cesi.ril19.javaproject.utils;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class ProjectNotFoundException extends ResponseStatusException {

    public ProjectNotFoundException(Long idProject) {
        super(HttpStatus.NOT_FOUND, "Project " + idProject + " doesn't exist!");
    }

}