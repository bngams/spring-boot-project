package fr.cesi.ril19.javaproject.services;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import fr.cesi.ril19.javaproject.entities.Project;
import fr.cesi.ril19.javaproject.entities.Task;
import fr.cesi.ril19.javaproject.repositories.ProjectsRepository;
import fr.cesi.ril19.javaproject.repositories.TasksRepository;
import fr.cesi.ril19.javaproject.repositories.UsersRepository;
import fr.cesi.ril19.javaproject.utils.ProjectNotFoundException;
import fr.cesi.ril19.javaproject.utils.UserNotFoundException;
import javassist.NotFoundException;

@Service
public class ProjectsService {

    // url from application properties
    @Value("${webservice-afternoon-url}")
    String urlConstAfterNoon;

    @Value("${webservice-fastshelf-url}")
    String urlConstFastShelf;

    ProjectsRepository projectsRepo;
    UsersRepository usersRepo;
    TasksRepository tasksRepo;

    DecimalFormat df = new DecimalFormat("#.#");

    @Autowired
    ProjectsService(ProjectsRepository projectsRepo, UsersRepository usersRepo, TasksRepository tasksRepo) {
        this.usersRepo = usersRepo;
        this.projectsRepo = projectsRepo;
        this.tasksRepo = tasksRepo;
        this.df.setRoundingMode(RoundingMode.HALF_UP);
    }

    public List<Project> getProjects() {
        return (List<Project>) this.projectsRepo.findAll();
    }

    public Project addProject(Project newProjectPojo) {
        Project newProject = new Project(newProjectPojo.getIduser(), newProjectPojo.getTitle(),
                newProjectPojo.getDescription(), newProjectPojo.getMode(), newProjectPojo.getBudget(),
                newProjectPojo.getStartdate(), newProjectPojo.getWorkdays());
        if (!this.usersRepo.existsById(newProject.getIduser())) {
            throw new UserNotFoundException(newProject.getIduser());
        }
        return this.projectsRepo.save(newProject);
    }

    public Project getProject(Long id) throws NotFoundException {
        return this.projectsRepo.findById(id)
                .orElseThrow(
                        () -> new ProjectNotFoundException(id));
    }

    public void deleteProject(Long id) {
        this.projectsRepo.findById(id)
                .orElseThrow(
                        () -> new ProjectNotFoundException(id));
        this.projectsRepo.deleteById(id);
    }

    public Project editProject(Project editProject) throws NotFoundException {
        this.projectsRepo.findById(editProject.getId())
                .orElseThrow(() -> new ProjectNotFoundException(editProject.getId()));
        if (!this.usersRepo.existsById(editProject.getIduser())) {
            throw new ResponseStatusException(HttpStatus.CONFLICT,
                    "User " + editProject.getIduser() + " doesn't exist!");
        }
        return this.projectsRepo.save(editProject);
    }

    public List<Project> getProjectBySearch(String word) {
        List<Project> searchProjects = (List<Project>) this.projectsRepo.searchProjectByKeyWord(word);
        if (searchProjects.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT);
        }
        return searchProjects;
    }

    public Double getTotalDays(Long idProject) {
        return calcTotalDays(this.projectsRepo.findById(idProject)
                .orElseThrow(() -> new ProjectNotFoundException(idProject)));
    }

    public Double getTotalCost(Long idProject) {
        return calcTotalCost(this.projectsRepo.findById(idProject)
                .orElseThrow(() -> new ProjectNotFoundException(idProject)));
    }

    public Double getBudgetRation(Long idProject) {
        return calcBudgetRation(projectsRepo.findById(idProject)
                .orElseThrow(() -> new ProjectNotFoundException(idProject)));
    }

    public void syncProject(Long idProject) { 
        if (!this.projectsRepo.existsById(idProject)) {
            throw new ProjectNotFoundException(idProject);
        }

        this.tasksRepo.cleanTasksByIdProject(idProject);
        new Thread(() -> {
            this.tasksRepo.saveAll(SyncWebServices.getTasksFastShelf(urlConstFastShelf, idProject));
        }).start();

        new Thread(() -> {
            this.tasksRepo.saveAll(SyncWebServices.getTasksAfterNoon(urlConstAfterNoon, idProject));
        }).start();
    }

    public List<Project> importAllProjects(@Valid List<Project> projects) {
        return (List<Project>) this.projectsRepo.saveAll(projects);
    }

    public Double calcTotalDays(Project project) {
        return Double.valueOf(df.format(project.getTasks().stream().mapToDouble(Task::getDuration).sum() / 7));
    }

    public Double calcTotalCost(Project project) {
        return Double.valueOf(df.format(
                project.getTasks().stream().mapToDouble(task -> task.getHourcost() * task.getDuration()).sum()));
    }

    public Double calcBudgetRation(Project project) {
        return Double.valueOf(df.format(calcTotalCost(project) / project.getBudget()));
    }
}
