package fr.cesi.ril19.javaproject.services;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.cesi.ril19.javaproject.entities.Project;
import fr.cesi.ril19.javaproject.entities.User;
import fr.cesi.ril19.javaproject.repositories.ProjectsRepository;
import fr.cesi.ril19.javaproject.repositories.UsersRepository;
import fr.cesi.ril19.javaproject.utils.UserNotFoundException;
import javassist.NotFoundException;

@Service
public class UsersService {

    UsersRepository usersRepo;
    ProjectsRepository projectsRepo;

    @Autowired
    UsersService(UsersRepository usersRepo, ProjectsRepository projectsRepo) {
        this.usersRepo = usersRepo;
        this.projectsRepo = projectsRepo;
    }

    public List<User> getAllUsers() {
        return (List<User>) this.usersRepo.findAll();
    }

    public User addUser(User newUser) {
        return this.usersRepo.save(newUser);
    }

    public User getUser(Long id) throws NotFoundException {
        return this.usersRepo.findById(id).orElseThrow(
                () -> new UserNotFoundException(id));
    }

    public User editUser(User newUser, Long id) throws NotFoundException {
        this.usersRepo.findById(id)
                .orElseThrow(
                        () -> new UserNotFoundException(id));
        return this.usersRepo.save(newUser);
    }

    public void deleteUser(Long id) throws NotFoundException {
        this.usersRepo.findById(id).orElseThrow(
                () -> new UserNotFoundException(id));
        this.usersRepo.deleteById(id);
    }

    public List<Project> getProjectsFromUserId(Long userId) {
        this.usersRepo.findById(userId).orElseThrow(
                () -> new UserNotFoundException(userId));
        return (List<Project>) this.projectsRepo.findByIdUser(userId);
    }

    public List<User> importAllUsers(@Valid List<User> users) {
        return (List<User>) this.usersRepo.saveAll(users);
    }
}
