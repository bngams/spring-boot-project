package fr.cesi.ril19.javaproject.services;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.cesi.ril19.javaproject.entities.Task;

@SuppressWarnings("unchecked")
public class SyncWebServices {

    public static List<Task> getTasksAfterNoon(String urlAfterNoon, Long idProject) {
        try {
            URL url = new URL(urlAfterNoon + idProject);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : " + conn.getResponseCode());
            }
            ObjectMapper mapper = new ObjectMapper();
            Map<String, Object> jsonMap = mapper.readValue(conn.getInputStream(), Map.class);
            ArrayList<Object> tasksJson = (ArrayList<Object>) jsonMap.get("tasks");
            List<Task> finalTask = tasksJson.stream().map(obj -> (Map<String, Object>) obj)
                    .map(mapObj -> new Task((String) mapObj.get("num"), Long.valueOf((int) mapObj.get("hourCost")),
                            Long.valueOf((int) mapObj.get("duration")), idProject))
                    .collect(Collectors.toList());
            conn.disconnect();
            return finalTask;
        } catch (Exception e) {
            System.out.println("Exception in SyncWebServices: " + e);
        }
        return null;
    }

    public static List<Task> getTasksFastShelf(String urlFastShelf, Long idProject) {
        try {
            URL url = new URL(urlFastShelf + idProject);
            List<Task> finalTasksList = new ArrayList<>();

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/xml,text/xml,application/xhtml+xml");
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : " + conn.getResponseCode());
            }
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
                Document document = docBuilder.parse(new InputSource(new StringReader(inputLine)));
                NodeList nodes = document.getElementsByTagName("item");
                for (int i = 0; i < nodes.getLength(); i++) {
                    finalTasksList.add(new Task(Long.valueOf(nodes.item(i).getChildNodes().item(0).getTextContent()),
                            "", Long.valueOf(nodes.item(i).getChildNodes().item(4).getTextContent().replace(".0", "")),
                            Long.valueOf(nodes.item(i).getChildNodes().item(7).getTextContent()) / 60, idProject));
                }
            }
            in.close();
            return finalTasksList;
        } catch (Exception e) {
            System.out.println("Exception in SyncWebServices: " + e);
        }
        return null;
    }
}