package fr.cesi.ril19.javaproject.controllers.api;

import java.io.IOException;
import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.sun.istack.NotNull;
import com.sun.istack.Nullable;

import fr.cesi.ril19.javaproject.entities.Project;
import fr.cesi.ril19.javaproject.services.ProjectsService;
import fr.cesi.ril19.javaproject.utils.CsvUtils;
import javassist.NotFoundException;

@RestController
@RequestMapping("/api/v1/projects")
public class ProjectsController {

    private ProjectsService projectsService;

    @Autowired
    ProjectsController(ProjectsService projectsService) {
        this.projectsService = projectsService;
    }

    @RequestMapping("")
    public List<Project> getAll() {
        return this.projectsService.getProjects();
    }

    @GetMapping("/{id}")
    public DetailsProject getProject(@PathVariable Long id) throws NotFoundException {
        return new DetailsProject(this.projectsService.getProject(id));
    }

    @PostMapping("")
    public Project addProject(@RequestBody Project newProject) throws NotFoundException {
        return this.projectsService.addProject(newProject);
    }

    @PutMapping("/{id}")
    public Project editProject(@RequestBody Project editProject) throws NotFoundException {
        return this.projectsService.editProject(editProject);
    }

    @DeleteMapping("{id}")
    public void deleteProject(@PathVariable Long id) {
        this.projectsService.deleteProject(id);
    }

    @GetMapping("/search")
    public List<Project> getProjectsBySearch(@RequestParam String word) { 
        return this.projectsService.getProjectBySearch(word);
    }

    @GetMapping("/{id}/stats")
    public StatsProject getStatsByProjectId(@PathVariable Long id) {
        return new StatsProject(this.projectsService.getTotalDays(id), this.projectsService.getTotalCost(id),
                this.projectsService.getBudgetRation(id), id);
    }

    @RequestMapping("/{id}/sync")
    public void syncProject(@PathVariable Long id) {
        this.projectsService.syncProject(id);
    }

    @PostMapping(path = "/import", consumes = "multipart/form-data")
    public List<Project> importProjects(@RequestBody MultipartFile csvBody) throws IOException {
        return this.projectsService.importAllProjects(CsvUtils.read(Project.class, csvBody.getInputStream()));
    }

    // POJO
    public class MinProject  {

        @NotNull
        @JsonProperty("title")
        public String title;

        @Nullable
        @JsonProperty("description")
        public String description;

        @Nullable
        @JsonProperty("mode")
        public String mode;

        @Nullable
        @JsonProperty("budget")
        public Long budget;

        @Nullable
        @JsonProperty("startdate")
        public Date startDate;

        @Nullable
        @JsonProperty("workdays")
        public Long workDays;

        public MinProject(String title, String description, String mode, Long budget, Date startDate,
                Long workDays) {
            this.title = title;
            this.description = description;
            this.mode = mode;
            this.budget = budget;
            this.startDate = startDate;
            this.workDays = workDays;
        }
    }

    @JsonPropertyOrder({ "id", "title", "mode", "budget", "startdate", "workdays" })
    public class DetailsProject extends MinProject {

        @NotNull
        @JsonProperty("id")
        public Long id;

        public DetailsProject(Project project) {
            super(project.getTitle(), project.getDescription(), project.getMode(), project.getBudget(),
                    project.getStartdate(), project.getWorkdays());
            this.id = project.getId();
        }
    }

    public class StatsProject {

        @JsonProperty("totalDays")
        public Double totalDays;

        @JsonProperty("totalCost")
        public Double totalCost;

        @JsonProperty("budgetRation")
        public Double budgetRation;

        @JsonProperty("project")
        public Long idProject;

        public StatsProject(Double totalDays, Double totalCost, Double budgetRation, Long idProject) {
            this.totalDays = totalDays;
            this.totalCost = totalCost;
            this.budgetRation = budgetRation;
            this.idProject = idProject;
        }
    }
}
