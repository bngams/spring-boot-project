package fr.cesi.ril19.javaproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CesiRil19Application {

    public static void main(String[] args) {
        SpringApplication.run(CesiRil19Application.class, args);
    }

}
