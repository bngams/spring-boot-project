package fr.cesi.ril19.javaproject.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.cesi.ril19.javaproject.entities.Project;

@Repository
public interface ProjectsRepository extends CrudRepository<Project, Long> {

    @Query(value = "SELECT * " + "FROM PROJECTS p " + "WHERE p.iduser=:idUser", nativeQuery = true)
    Iterable<Project> findByIdUser(Long idUser);

    @Query(value = "SELECT * " + "FROM PROJECTS p " + "WHERE LOWER(p.title) LIKE LOWER(CONCAT('%',:word,'%')) "
            + "OR LOWER(p.description) LIKE LOWER(CONCAT('%',:word,'%')) "
            + "ORDER BY p.budget ASC", nativeQuery = true)
    Iterable<Project> searchProjectByKeyWord(String word);

}
