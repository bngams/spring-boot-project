package fr.cesi.ril19.javaproject.entities;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "PROJECTS")
@Data
@NoArgsConstructor
public class Project {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private Long iduser;

    private String title;

    private String description;

    private String mode;

    private Long budget;

    private Date startdate;

    private Long workdays;

    // can fail toString method
    @ToString.Exclude
    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<Task> tasks = new HashSet<>();

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "iduser", insertable = false, updatable = false, nullable = false)
    @JsonIgnore
    private User user;

    public Project(Long idUser, String title, String description, String mode, Long budget, Date startDate,
            Long workDays) {
        this.iduser = idUser;
        this.title = title;
        this.description = description;
        this.mode = mode;
        this.budget = budget;
        this.startdate = startDate;
        this.workdays = workDays;
    }
}