package cesi.ril19.javaproject.cesiRIL19;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.RoundingMode;
import java.sql.Date;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.HashSet;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import fr.cesi.ril19.javaproject.CesiRil19Application;
import fr.cesi.ril19.javaproject.entities.Project;
import fr.cesi.ril19.javaproject.entities.Task;
import fr.cesi.ril19.javaproject.services.ProjectsService;

@SpringBootTest(classes = CesiRil19Application.class)
class StatsTests {

    DecimalFormat df = new DecimalFormat("#.#");

    @Autowired
    ProjectsService projectsService;

    Project project;

    @BeforeEach
    void setUp() throws Exception {
        project = new Project(99L, "A", "B", "C", 10L, new Date(10L), 101L);
        project.setTasks(new HashSet<Task>(
                Arrays.asList(new Task("", 12L, 13L, 99L), new Task("", 53L, 99L, 99L), new Task("", 75L, 23L, 99L))));
        this.df.setRoundingMode(RoundingMode.HALF_UP);
    }

    @Test
    void totalDaysTest() {
        Double resultExpected = (13.0 + 99.0 + 23.0) / 7.0;
        assertThat(this.projectsService.calcTotalDays(this.project))
                .isEqualTo(Double.valueOf(df.format(resultExpected)));
    }

    @Test
    void totalCostTest() {
        Double resultExpected = (12.0 * 13.0) + (53.0 * 99.0) + (75.0 * 23.0);
        assertThat(this.projectsService.calcTotalCost(this.project))
                .isEqualTo(Double.valueOf(df.format(resultExpected)));
    }

    @Test
    void budgetRationTest() {
        Double resultExpected = ((12.0 * 13.0) + (53.0 * 99.0) + (75.0 * 23.0))
                / Double.valueOf(this.project.getBudget());
        System.out.println("budgetRationTest" + resultExpected);
        assertThat(this.projectsService.calcBudgetRation(this.project))
                .isEqualTo(Double.valueOf(df.format(resultExpected)));
    }

}
